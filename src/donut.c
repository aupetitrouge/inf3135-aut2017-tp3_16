#include "constants.h"
#include "donut.h"

struct Donut *Donut_create(SDL_Renderer *renderer) {
	struct Donut *donut;
	donut = (struct Donut*)malloc(sizeof(struct Donut));
	donut->renderer = renderer;
	donut->screenPosition.x = MAP_INITIAL_X;
	donut->screenPosition.y = MAP_INITIAL_Y;
	donut->mapPosition.x = 0;
	donut->mapPosition.y = 0;
	donut->found = false;
	donut->animatedSpritesheet =
		AnimatedSpritesheet_create(DONUT_SPRITESHEET, 1, 32, 32,
									DONUT_BETWEEN_FRAME, renderer);
	donut->animatedSpritesheet->spritesheet->scale = DONUT_SCALE;
	return donut;
}

void Donut_delete(struct Donut *donut)
{
	if (donut != NULL) {
		AnimatedSpritesheet_delete(donut->animatedSpritesheet);
		free(donut);
	}
}

void Donut_render(struct Donut *donut)
{
	AnimatedSpritesheet_render(donut->animatedSpritesheet,
								donut->screenPosition.x,
								donut->screenPosition.y);
	AnimatedSpritesheet_run(donut->animatedSpritesheet);
}

void Donut_location_assigner(double x, double y, struct Donut *donut)
{
	donut->mapPosition.x = x;
	donut->mapPosition.y = y;
	donut->screenPosition.x = x;
	donut->screenPosition.y = y;
	donut->hitbox.x = x;
	donut->hitbox.y = y;
	donut->hitbox.w = DONUT_SIZE;
	donut->hitbox.h = DONUT_SIZE;
}
